package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage extends Page {


    //selectors
    private By searchFieldSelector = By.name("q");
    public By userPanelSelector = By.className("siteHeader__personal");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public BookDetailsPage searchBook(String name){
        SearchList searchList = this.goToList(name);
        searchList.openFirstBook();
        return  new BookDetailsPage(driver);
    }

    public SearchList goToList(String name){
        WebElement searchField =  driver.findElement(searchFieldSelector);
        searchField.sendKeys(name);
        searchField.submit();

        return new SearchList(driver);
    }
}
