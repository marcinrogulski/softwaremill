package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {

    protected WebDriver driver;

    public By unshelveSelector = By.xpath("//button[contains(@title, 'Remove this book from your shelves')]");
    public By myBooksSelector = By.linkText("My Books");

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void waitUntilItemsDisplayed(By locator) {
        //wait until item shows on page
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public MyBooksPage gotToMyBooks(){
        driver.findElement(myBooksSelector).click();
        return  new MyBooksPage(driver);
    }

}
