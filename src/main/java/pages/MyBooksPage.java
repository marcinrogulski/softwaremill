package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyBooksPage extends Page {


    public MyBooksPage(WebDriver driver) {
        super(driver);
    }

    public WebElement book(String title){
        return driver.findElement(By.xpath("//a[contains(@title, '" + title + "')]"));
    }

}
