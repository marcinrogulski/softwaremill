package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {

    private final String url = "https://www.goodreads.com/";

    //selectors
    private By emailFieldLocator = By.id("userSignInFormEmail");
    private By passwordFieldLocator = By.id("user_password");
    private By signInButtonLocator = By.xpath("//input[@value='Sign in']");
    public By errorSelector = By.xpath("//div[@id='emailForm']/p");



    public LoginPage(WebDriver driver) {
        super(driver);
        driver.get(url);
    }

    public MainPage login(String email, String password){
        driver.findElement(emailFieldLocator).sendKeys(email);
        driver.findElement(passwordFieldLocator).sendKeys(password);
        driver.findElement(signInButtonLocator).click();
        return new MainPage(driver);
    }

}
