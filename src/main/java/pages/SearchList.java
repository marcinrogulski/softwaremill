package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchList extends Page {

    //selectors
    private By firstBookSelector = By.xpath("//a[contains(@class, 'bookTitle')][1]");

    public SearchList(WebDriver driver) {
        super(driver);
    }

    public BookDetailsPage openFirstBook(){
        driver.findElement(firstBookSelector).click();
        return new BookDetailsPage(driver);
    }

    public void wantToRead(Integer number){
        driver.findElement(By.xpath("//button[contains(@class, 'wtrToRead')]["+ number + "]")).click();
        waitUntilItemsDisplayed(unshelveSelector);

    }
}
