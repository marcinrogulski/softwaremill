package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookDetailsPage extends Page {

    //selectors
    private By wantToReadSelector = By.className("wtrToRead");


    public BookDetailsPage(WebDriver driver) {
        super(driver);
    }

    public void wantToRead() {
        driver.findElement(wantToReadSelector).click();
        waitUntilItemsDisplayed(unshelveSelector);
    }

    public void removeFromShelve(){
        driver.findElement(unshelveSelector).click();

    }

}
