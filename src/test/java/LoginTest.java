import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import pages.LoginPage;
import pages.MainPage;

public class LoginTest extends TestCase {


    @Test
    public void  basicLoginTest(){
        LoginPage loginPage = new LoginPage(driver);
        MainPage mainPage = loginPage.login("marcinr123@onet.eu", "cFMsgU3fiaXDaUp");
        Assert.assertTrue(driver.findElement(mainPage.userPanelSelector).isDisplayed());
    }

    @Test
    public void  invalidCredentialsTest(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("xxx@xxx.com", "xxx");
        WebElement error = driver.findElement(loginPage.errorSelector);
        Assert.assertTrue(error.isDisplayed());
    }
}
