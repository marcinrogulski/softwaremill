import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pages.*;

public class WantReadTest extends TestCase {

    private String firtsBook = "Tango";
    private String secondBook = "Devotion";

    @Before
    public void login(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("marcinr123@onet.eu", "cFMsgU3fiaXDaUp");
    }

    @Test
    public void  basicWantToReadTest(){
        MainPage mainPage = new MainPage(driver);
        BookDetailsPage bookDetailsPage = mainPage.searchBook(firtsBook);
        bookDetailsPage.wantToRead();

        Assert.assertTrue(driver.findElement(bookDetailsPage.unshelveSelector).isDisplayed());

        MyBooksPage myBooksPage = bookDetailsPage.gotToMyBooks();
        Assert.assertTrue(myBooksPage.book(firtsBook).isDisplayed());
    }

    @Test
    public void  wantToReadFromListTest(){
        MainPage mainPage = new MainPage(driver);
        SearchList searchList = mainPage.goToList(secondBook);
        searchList.wantToRead(1);

        Assert.assertTrue(driver.findElement(searchList.unshelveSelector).isDisplayed());

        MyBooksPage myBooksPage = searchList.gotToMyBooks();
        Assert.assertTrue(myBooksPage.book(secondBook).isDisplayed());
    }

}
